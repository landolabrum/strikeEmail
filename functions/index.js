var sendsms = require('./server-fns').sendsms;
var generateToken = require('./server-fns').generateToken;
var cors = require("cors")({origin: true});
// The Cloud Functions for Firebase SDK to create Cloud Functions and setup
// triggers.
const functions = require('firebase-functions');

exports.brainTreeClientToken = functions
    .https
    .onRequest((req, res) => {
        cors(req, res, () => {
           generateToken(req, res)
        })
    });
exports.sendsms = functions
    .https
    .onRequest((req, res) => {
        cors(req, res, () => {
            sendsms(req, res)
        })
    });