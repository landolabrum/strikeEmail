var path = require('path');
var braintree = require('braintree');
var fs = require('fs');

var FROM_NUMBER = '+14357315068';
const EMPTY_TEST = /^(\s*|\/\/.*)$/
var quotes = fs
  .readFileSync(__dirname + '/txt.txt', 'UTF8')
  .split('\n')
  .filter((str) => {
    return !EMPTY_TEST.test(str)
  })
var images = fs
  .readFileSync(__dirname + '/img.txt', 'UTF8')
  .split('\n')
  .filter((str) => {
    return !EMPTY_TEST.test(str)
  })
console.log(quotes)
console.log(images)
var gateway = braintree.connect({environment: braintree.Environment.Production, merchantId: 'g2wgcy9cct85ydr7', publicKey: '62886mctxtwg2sfg', privateKey: '3699bc90ef24fc7affbde15d9e465bb0'});

const keys = {
  sid: 'AC301224669b919a94a3f9a1f70ac7b900',
  token: 'd67cdfeec6a3a6bb711ffef984c58554'
}
var client = require('twilio')(keys.sid, keys.token);
module.exports.generateToken = (req, res) => {
  gateway
    .clientToken
    .generate({}, function (err, response) {
      if (err) {
        return res
          .status(err.code || 500)
          .send(err.message || err);
      }
      res.send(response.clientToken);
    });
}
module.exports.sendsms = (req, res) => {
  var amount = calculateAmount(req.body)
  Promise
    .resolve(req.body.nonce)
    .then((nonceFromTheClient) => {
      return new Promise(function (resolve, reject) {
        gateway
          .transaction
          .sale({
            amount: amount.toString(),
            paymentMethodNonce: nonceFromTheClient,
            options: {
              storeInVaultOnSuccess: true
            }
          }, function (err, result) {
            if (err) 
              return reject(err);
            if (!(result.success || result.transaction)) {
              return reject(result.errors.deepErrors())
            }
            resolve(result)
          });
      })
    })
    .then((reciept) => {
      return sendRapidFire(req.body.numMessages, req.body.recipient).then(() => {
        return timeOut(1000 * 3)
      }).then(() => {
        return sendOne(req.body.recipient, createMessage(req.body))
      }).then(() => {
        return sendOne(req.body.recipient, "sent from: " + req.body.name)
      }).then((responseData) => {
          res.json({"From": responseData.from, "Body": responseData.body, "reciept": reciept});
        })
    })
    .catch((error) => {
      console.error(error)
      res.status(500)
      res.send(error)
    })
};

function calculateAmount(body) {
  console.log(body)
  var amount = 0
  if (body.numMessages === '10') {
    amount = amount + 0.99
  }
  if (body.numMessages === '20') {
    amount = amount + 1.79
  }
  if (body.numMessages === '30') {
    amount = amount + 2.49
  }
  if (body.numMessage !== '50') {
    amount = amount + 3.99
  }
  if (body.customMessage !== '') {
    amount = amount + 0.10
  }
  if (amount < 0.5) 
    throw 'not enough moneyz!'
  return Math.floor(amount * 100) / 100
}
function createMessage(body) {
  if (!body.customMessage || body.customMessage === '') {
    return 'Keep attacking!'
  } else {
    return body.customMessage
  }
}
function chooseRandomImageAndQuote(quotes, images) {
  var quote = quotes[Math.floor(quotes.length * Math.random())];
  var image = images[Math.floor(images.length * Math.random())];
  return {quote, image};
}

function sendRapidFire(numMessages, recipient) {
  return Promise.all((function () {
    var ps = [];
    for (var i = 0; i < numMessages; i++) {
      var {quote, image} = chooseRandomImageAndQuote(quotes, images);
      ps.push(sendOne(recipient, quote, image));
    }
    return ps;
  })())
}

// function sendRapidFire(numMessages, recipient) {
//   var copyQuotes = quotes.slice()
//   var copyImages = images.slice()
//   return Promise.all((function () {
//     var ps = [];
//     for (var i = 0; i < numMessages; i++) {
//       var {quote, image} = chooseRandomImageAndQuote(copyQuotes, copyImages);
//       ps.push(sendOne(recipient, quote, image));
//     }
//     return ps;
//   })())

// }

function sendOne(recipient, body, mediaUrl) {
  var message = {
    to: recipient,
    from: FROM_NUMBER,
    body: body
  }
  if (mediaUrl) 
    message.mediaUrl = mediaUrl
  return new Promise(function (res, rej) {
    client
      .sendMessage(message, function (err, data) {
        if (err) 
          return rej(err);
        res(data)
      })
  })
}
function sendDelayedFire(numMessages, recipient, timeout) {
  return fire(0);

  function fire(i) {
    if (i === numMessages) 
      return;
    var {quote, image} = chooseRandomImageAndQuote(quotes, images);
    return sendOne(recipient, quote, image).then(function () {
      return new Promise(function (res) {
        setTimeout(res, timeout);
      });
    })
      .then(function () {
        return fire(i + 1);
      });
  }
}
function timeOut(num) {
  return new Promise((res) => {
    setTimeout(res, num)
  })
}