import React, {Component} from 'react';

export default class ListOrCreate extends Component {
    constructor(a, b, c) {
        super(a, b, c);
        this.state = {
            items: []
        };
    }

    createItem(value) {
        window
            .$('#createFormModal')
            .modal('hide');
    }
    render() {
        return (
            <div>
                <div
                    className='modal fade'
                    id='createFormModal'
                    tabIndex='-1'
                    role='dialog'
                    aria-hidden='true'>
                    <div className='fluid-container'>
                        <div className='row'>
                            <div className='col-xs-3'>
                                <button
                                    type='button'
                                    className='closeLan'
                                    data-dismiss='modal'
                                    aria-label='Close'>
                                    <span className='glyphicon glyphicon-menu-left'/>
                                </button>
                            </div>
                        </div>
                        <div className='container'>
                            <CreateForm
                                submitForm={this.props.submitForm}
                                isValid={this.props.isValid}
                                formSuccess={(value) => {
                                this.createItem(value);
                            }}
                                formFailed={(error) => {
                                console.log('formSubmitError', error);
                            }}>{this.props.children}</CreateForm>
                        </div>
                    </div>
                </div>
                <div className='container'>
                    <h3>Saved cards</h3><hr/>
                    {this
                        .props
                        .items
                        .map((item, i) => {
                            return (
                                    <div
                                        key={i}
                                        className='btn btn-left btn-block btn-default rad'
                                        onClick={() => this.props.chooseItem(item)}>
                                        <img src={item.imageUrl}/>
                                        <b className='pull-right'>
                                            **** **** **** {item.last4}</b>
                                    </div>
                            );
                        })
}
                    <hr/>
                    <button
                        className='btn btn-block btn-lg btn-success'
                        data-toggle='modal'
                        data-target='#createFormModal'>
                        Add new payment method
                    </button>

                </div>
            </div>
        );
    }
}
function CreateForm(props) {
    var submitting = false;
    return (
        <form
            className='col-md-6 col-md-offset-3'
            action='/'
            onSubmit={(e) => {
            e.preventDefault();
            if (submitting) 
                return false;
            submitting = true;
            props
                .submitForm()
                .then((value) => {
                    return props.formSuccess(value);
                })
                .catch((e) => {
                    props.formFailed(e);
                    submitting = false;
                });
        }}>
            {props.children}
            <hr/>
            <button
                className='btn btn-block btn-warning'
                type='submit'
                disabled={!props.isValid || submitting}>Pay</button>
        </form>
    );
}