import React, {Component} from "react";
import braintree, {hostedFields} from 'braintree-web';
import {getToken} from './client-id-store';

export default class PaypalButton extends Component {
  constructor() {
    super();
    this.state = {
      componentID: Date
        .now()
        .toString(),
      creditCards: []
    };
  }
  componentDidMount() {
    getToken().then((client) => {
      return new Promise((resolve, reject) => {
        braintree
          .paypal
          .create({
            client: client
          }, (error, instance) => {
            if (error) 
              return reject(error)
            resolve(instance)
          })
      })
    }).then((instance) => {
      this.setState({instance: instance})
    }).catch((error) => {
      console.error(error)
    })
  }
  handleSubmit() {
    return new Promise((resolve, reject) => {
     
    })
  }
  render() {
    const {} = this.state;
    var submitting = false;
    return (
      <a> Paypal</a>
    )
  }
};
