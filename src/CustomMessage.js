import React from 'react';
import 'font-awesome-webpack';

export default class LoginControl extends React.Component {
  constructor(props) {
    super(props);
    this.handleShowClick = this
      .handleShowClick
      .bind(this);
    this.handleHideClick = this
      .handleHideClick
      .bind(this);
    this.state = {
      isShow: true
    };
  }

  handleShowClick() {
    this.setState({isShow: true});
  }

  handleHideClick() {
    this
      .props
      .onChange({
        target: {
          value: ''
        }
      })
    this.setState({isShow: false});
  }

  render() {
    const isShow = this.state.isShow;
    let button = null;
    if (isShow) {
      button = <HideButton onClick={this.handleHideClick}/>;
    } else {
      button = <ShowButton onClick={this.handleShowClick}/>;
    }
    return (
      <div>
      
          {button}
      
        <br/>
        <View
          onChange={this.props.onChange}
          isShow={isShow}
          value={this.props.value}/>
      </div>
    );
  }
}
function View(props) {
  const isShow = props.isShow;
  if (isShow) {
    return <span>
        <input
        placeholder='Custom Message'
          onChange={props.onChange}
          value={props.value}
          maxLength='100'
          type='text'
          id='textarea1'
          className='materialize-textarea'></input>
        <label htmlFor='textarea1'>
          Custom message
        </label>
     </span>;
  }
  return <span />;
}

function ShowButton(props) {
  return (
    <a  className='grey-text' onClick={props.onClick} > Custom message + $0 .10 <i className='fa fa-square-o'/></a>
  );
}

function HideButton(props) {
  return (

   
   <a className='grey-text'> Custom message + $0 .10 <i className='fa fa-check-square-o' onClick={props.onClick} /></a>
    

  );
}