import React from 'react';
import ReactDOM from 'react-dom';
import 'whatwg-fetch';
import BrainTreeComponent from './BrainTreeComponent';
import PaypalButton from './PaypalButton';
import CustomMsg from './CustomMessage';
import 'font-awesome-webpack';

const messageNumberOptions = [10, 20, 30, 50]

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: [],
      recipient: '',
      numMessages: 30,
      customMessage: ''
    };
  }
  changeInput(event) {
    this.setState({recipient: event.target.value});
  }
  sendSms(nonce) {
   
  }
  componentDidMount() {

   

  }
  handleRadioButton(numMessages) {
   
  }
  render() {
    return (
      <span>
        <div className="navbar-fixed">
          <nav>
            <div className="nav-wrapper">
              <img
                src="https://storage.googleapis.com/trumpstrikes-3069c.appspot.com/trump-2546104.jpg"
                className="responsive-img"/></div>
          </nav>
        </div>

        <div className='container'>
          <div className="row ">

            <div className="col s12 m6">
              <div className='header'>
                <img className='responsive-img'
                  src="https://storage.googleapis.com/trumpstrikes-3069c.appspot.com/trump108020.png"/>
               
              </div>
              <div className='card white'>

                <div className='card-content'>
                <p className='grey-text'><b>Attack your friends and family
                <i> anonymously </i>
                with 10, 20, 30 or 50 text message Trump Strikes to mess with their emotional
                well-being as a hilarious prank</b></p><br/>
                  <div className='input-field'>
                    <input
                      type='tel'
                      id='tel'
                      onChange={(event) => {
                      this.setState({recipient: event.target.value});
                    }}
                      value={this.state.recipient}/>
                    <label htmlFor='tel'>Phone Number to Strike</label>
                  </div>
                  <div>
                  {messageNumberOptions.map((numMessages) => {
                    return (
                      <p onClick={() => this.handleRadioButton(numMessages)}>
                        <input
                          checked={this.state.numMessages === numMessages}
                          type='radio'
                          value={numMessages}
                          name='numMessages'/>
                        <label>
                          {numMessages} Messages
                        </label>
                      </p>
                    )
                  })}
                </div>

                  <br/>

                  <CustomMsg
                    onChange={(event) => {
                    this.setState({customMessage: event.target.value});
                  }}
                    value={this.state.customMessage}/>

                  
                  <div className='input-field'>
                  <input
                    id='first_name'
                    type='text'
                    className='validate'
                    onChange={(event) => {
                    this.setState({name: event.target.value});
                  }}
                    value={this.state.name}/>
                  <label htmlFor='first_name'>Name</label>
                </div>

                  <div className='card'>
                    <CashControl
                      onResolve={(n) => {
                      this.sendSms(n)
                    }}/>
                  </div>
                  <p>We store no data and never see your credit card. All transactions processed
                    through
                    <a href='http://braintreepayments.com' target='_blank'>
                      BraintreePayments.com</a>. PayPal charges appear from 'Ratio Networking'.</p>
                  <p >In order to comply with U.S. law, your friend needs to know your name in
                    order to comply with the 'CAN-SPAM Act'</p>
                  <p >US numbers only please. Standard text rates apply and trumpstrikes.com is
                    not responsible for any such fees, so please use responsibly.</p>

                  <h6>
                    <p>
                      <small>©2017 Strikes Media, Heber City, UT. Access & use of this website is
                        subject to the Privacy Policy and Terms and Conditions.</small>
                    </p>
                    <p>
                      <small>
                        Stay Updated! Links to other Web sites are available for your convenience.
                        Strikes Media has no control over, does not necessarily endorse, and is not
                        responsible for the content, advertising, products or other materials on or
                        available from these Web sites.
                      </small>
                    </p>
                    <p>
                      <small>All Rights Reserved.</small>
                    </p>
                  </h6>
                  <div className='modal' id='modalMessage'>

                    <div className="modal-content">
                      <h4>Modal Header</h4>
                      <h3>{this.state.modalMessage}</h3>
                    </div>
                    <div className="modal-footer">
                      <a className="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col s12 m6">
              <div className='card'>
                <div className="card-content grey lighten-4"></div>
              </div>

            </div>
          </div>

        </div>

      </span>
    );
  }
}
class CashControl extends React.Component {

  constructor(props) {
    super(props);
    this.handleCashClick = this
      .handleCashClick
      .bind(this);
    this.handlePayPalClick = this
      .handlePayPalClick
      .bind(this);
    this.state = {
      isCash: true
    };
  }

  handleCashClick() {
    this.setState({isCash: false});
  }

  handlePayPalClick() {
    this.setState({isCash: true});
  }

  render() {
    const isCash = this.state.isCash;

    let button = null;
    if (isCash) {
      button = <div className='card-tabs'>

        <PayPalButton onClick={this.handlePayPalClick}/>
      </div>;
    } else {
      button = <div className='card-tabs'>
        <CashButton onClick={this.handleCashClick}/>
      </div>;
    }
    return (
      <div>
        {button}
        <Display onResolve={this.props.onResolve} isCash={isCash}/>
      </div>
    );
  }
}

function CashDisplay(props) {
  return <div className='card-content grey lighten-4'>
    <BrainTreeComponent onResolve={props.onResolve}/>
  </div>;
}

function PayPalDisplay(props) {
  return <PaypalButton onResolve={props.onResolve}/>;
}

function Display(props) {
  const isCash = props.isCash;
  if (isCash) {
    return <CashDisplay onResolve={props.onResolve}/>;
  }
  return;
}

function CashButton(props) {
  return (
    <ul className='tabs tabs-fixed-width'>
      <li className='tab'>
        <a onClick={props.onClick}>
          Card
        </a>
      </li>
      <li className='tab'>
        <a>
          PayPal
        </a>
      </li>
    </ul>
  );
}

function PayPalButton(props) {
  return (
    <ul className='tabs tabs-fixed-width '>
      <li className='tab'>
        <a className='blue-text lighten-1'>
         Card
        </a>
      </li>
      <li className='tab'>
        <PayPalDisplay/>
      </li>
    </ul>
  );
}

ReactDOM.render(
  <App/>, document.getElementById('root'));
