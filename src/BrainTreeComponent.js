import React, {Component} from 'react';
import braintree, {hostedFields} from 'braintree-web';
import {getToken} from './client-id-store';

export default class BrainTreeComponent extends Component {
  constructor() {
    super();
    this.state = {
      
    };
  }
  componentDidMount() {
    getToken().then((client) => {
      hostedFields.create({
        client: client,
        styles: {
          'input': {
            'font-size': '16pt',
            'color': '#3A3A3A'
          },
          '.number': {
            'font-family': 'monospace'
          },
          '.valid': {
            'color': 'green'
          }
        },
        fields: {
          number: {
            selector: '#card-number',
            placeholder: 'xxxx-xxxx-xxxx-xxxx'
          },
          cvv: {
            selector: '#cvv',
            placeholder: 'xxx'
          },
          expirationDate: {
            selector: '#expiration-date',
            type: 'month'
          }
        }
      }, (err, hostedFieldValues) => {
        if (err) {
          return console.error(err);
        }
        this.setState({hostedFieldValues: hostedFieldValues});
      });
    })
      .catch(function (err) {
        console.error(err);
      });
  }
  handleSubmit() {
    
  }
  render() {
    const {finishedState, submitting, componentID, hostedFieldValues, response} = this.state;

    return (
      <form
        >
        <div className='row col s12'>
          <div className='row'>
            <div className='input-field col s12'>Card number:
              <div className='' id='card-number'/>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>CVV:
             
              <div className='' id='cvv'/>
            </div>
          </div>
          <div className='row'>
            <div className='input-field col s12'>Expire:
              <div className='' id='expiration-date'/>
            </div>
          </div>
        </div>
        <button
          className='btn btn-floating btn-large pulse green'
          >STRIKE</button>
      </form>
    )
  }
};