import express from 'express';
import bodyParser from 'body-parser';
import { sendsms } from './functions/server-fns'

const app = express();

app.use('/', express.static('public'));

app.post('/sendsms', bodyParser.json(), sendsms) 
app.listen(process.env.PORT || 3000);